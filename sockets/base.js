/**
 * Created by ukp on 03/11/18.
 */

module.exports = function (io) {

    io.on('connection', function (socket) {
        console.log('New User connected. Socket ID:', socket.id);


        socket.on('chat', function (data) {
            // socket.emit('chat', data); //For Single Connection reply
            io.sockets.emit('chat', data);
        });


        socket.on('disconnect', function(){
            console.log('User disconnected with socket ID:', socket.id);
        });
    });
}

